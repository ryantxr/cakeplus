<?php
/* Converts a vanilla CakePHP install and adds some basic
 * extensions.
 */
define('DS', DIRECTORY_SEPARATOR);
define('LT', '<');
define('PHPBEGIN', LT . '?php');
define('PHPEND', '?>');

class FileSystemObject{
    public $path;
    public $file; // full path to file    
}
class Dir extends FileSystemObject {
    public function __construct($filespec){
        $this->path = $filespec;
    }

    function exists(){
        return is_dir($this->path);
    }    
}
class AFile extends FileSystemObject {
    public $loaded = false;
    public $dirty = false;
    public $content = null;

    public function __construct($filespec){
        $this->file = $filespec;
        $this->path = dirname($filespec);
    }
    function exists(){
        return file_exists($this->file);
    }
    function load(){
        //echo ">>>>>> loading file "  . $this->file ."\n";
        $this->content = file_get_contents($this->file);
        $this->loaded = true;
        //echo ">>> contents:\n" . 
        //    $this->content .
        //    "\n<<<\n";
    }
    function save(){
        file_put_contents($this->file, $this->content);
        $this->dirty = false;
    }
    function clear(){
        $this->content = null;
        $this->dirty = false;
        $this->loaded = false;
    }
}

class CakePhpEnhancer{

    private $cakeOk = false;
    public $args = array();
    public $checkShouldNotExist = true;

    static $instance;
    static function getInstance(){
        if ( self::$instance ) return self::$instance;
        self::$instance = new CakePhpEnhancer;
        return self::$instance;
    }
    static function start($args, $dir){
        $instance = self::getInstance();
        $instance->args = $args;
        if ( count($instance->args) < 2 ){
            print "execute or core\n";
            exit;
        }
        switch($instance->args[1]){
            case 'execute':
                $instance->execute($dir);
                break;
            case 'core':
                $instance->executeCore($dir);
                break;
            default:

                break;
        }
    }

    public function execute($dir){
        echo "Starting\n";
        $this->init($dir);
        $this->checkForCake($dir);

        $this->bootstrapChanges();
        $this->coreChanges();

        $this->databaseConfigFile();
        $this->appControllerChanges();
        
        $this->createUserController();
        $this->createUserModel();
        $this->createUserView();

        $this->createHomeController();
        $this->createHomeViews();

        $this->homePageChanges();
        $this->cssChanges();
        $this->layoutChanges();

    }

    public function executeCore($dir){
        echo "Starting\n";
        $this->checkShouldNotExist = false;
        $this->init($dir);
        $this->checkForCake($dir);

        $this->coreChanges();        
    }

    public function coreChanges(){

        $this->core = new stdclass;
        $this->core->dirty = false;
        $this->core->contents = file_get_contents($this->Config->coreFile);
        $this->core->loaded = true;

        $buffer =  explode("\n", $this->core->contents);

        $lineCount = count($buffer);
        $bufferChange = false;
        for($i=0; $i<$lineCount;$i++){

            if ( strpos($buffer[$i], 'Configure::write(\'Security.salt') !== false ){
                echo "Found salt\n";
                echo ">>>>> ". $buffer[$i] . "\n";
                $saltRand = substr(base64_encode(md5(time().rand(10000, 99999))), 0, 4 );
                $buffer[$i] = preg_replace('/(salt\', +)\'..../', '$1\''.$saltRand, $buffer[$i]);
                echo ">>>>> ". $buffer[$i] . "\n";
                $bufferChange = true;
            }
            if ( strpos($buffer[$i], 'Configure::write(\'Security.cipherSeed') !== false ){
                echo "Found cipherSeed\n";
                echo ">>>>> ". $buffer[$i] . "\n";
                $search = 'Seed\', *\'[.]';
                //$buffer[$i] = preg_replace('/(Seed\', +\',[.]+)\'[.]/', '$1\'_', $buffer[$i]);
                $seedRand = rand(1000, 9999);
                $buffer[$i] = preg_replace('/(Seed\', *)\'..../', '$1\''.$seedRand, $buffer[$i]);
                echo ">>>>> ". $buffer[$i] . "\n";
                $bufferChange = true;
            }
        }
        if ( $bufferChange ){
            $this->core->contents = implode("\n", $buffer);
            $this->core->dirty = true;
        }

        // If it was changed then save the file
        if ( $this->core->dirty ){
            echo "OK. Updated core.\n";
            file_put_contents($this->Config->coreFile, $this->core->contents);
            $this->core->contents = null;
            $this->core->loaded = false;
            $this->core->dirty = false;
        }

    }

    public function bootstrapChanges(){
        $this->bootstrap = new stdclass;
        $this->bootstrap->dirty = false;
        $this->bootstrap->contents = file_get_contents($this->Config->bootstrapFile);
        $this->bootstrap->loaded = true;

        // Make various changes
        $this->disableInflection();

        // If it was changed then save the file
        if ( $this->bootstrap->dirty ){
            echo "OK. Updated bootstrap\n";
            file_put_contents($this->Config->bootstrapFile, $this->bootstrap->contents);
            $this->bootstrap->contents = null;
            $this->bootstrap->loaded = false;
            $this->bootstrap->dirty = false;
        }
    }


    public function disableInflection(){
        echo "Disable inflection\n";

        if ( ! $this->bootstrap->loaded ){
            throw new Exception('Bootstrap file was not loaded');
        }

        $inflection = array(
            '// inflection disabled',
            'Inflector::rules(\'singular\', array(\'rules\' => array(\'/(.*)/\' => \'$1\'), \'irregular\' => array(), \'uninflected\' => array()));',
            'Inflector::rules(\'plural\', array(\'rules\' => array(\'/(.*)/\' => \'$1\'), \'irregular\' => array(), \'uninflected\' => array()));',
            '// end inflection disabled');

        $buffer = explode("\n", $this->bootstrap->contents);
        $lineCount = count($buffer);
        $state = 0;
        $inflectionLine = null;
        for($i=0; $i<$lineCount;$i++){

            if ( strpos($buffer[$i], 'Inflector::rules') !== false ){
                $state = 1;
            }
            if ( $state == 1 ){
                if ( preg_match('/\*\\//', $buffer[$i]) ){
                    $inflectionLine = $i + 1;
                    $state = 0;
                }
            }
        }
        if ( $inflectionLine ){
            //echo "Inflection insert point " . $inflectionLine . "\n";
            array_splice($buffer, $inflectionLine+1, 0, $inflection);
            //print_r( $buffer );
            $this->bootstrap->contents = implode("\n", $buffer);
            $this->bootstrap->dirty = true;
        }
    }   

    public function databaseConfigFile(){
        echo "Make a database.php\n";

        $contents = file_get_contents($this->Config->databaseDefaultFile);

        file_put_contents($this->Config->databaseFile, $contents);

    }

    public function appControllerChanges(){
        
        $search = 'class AppController';
        //$this->Controller->appController->file;
        $this->Controller->appController->content = file_get_contents($this->Controller->appController->file);
        $this->Controller->appController->loaded = true;


        $buffer = explode("\n", $this->Controller->appController->content );
        $lineCount = count($buffer);
        $state = 0;
        $insertPoint = 0;
        for($i=0; $i < $lineCount; $i++){
            if ( $state == 1 ) $state = 2;
            if (  strpos($buffer[$i], $search) !== false ){
                $state = 1;
            }
            //echo "Add AuthComponent to AppController\n";
            //echo "Add beforeFilter to AppController\n";
        

            if ( $state == 2 && trim($buffer[$i]) == '}' ){

                //echo "$insertPoint: {$buffer[$i]}\n";
                $insertPoint = $i;

            }
        }
        $insertCode = 
        '    public $components = array(\'Auth\', \'Session\');

    public function beforeFilter(){
        $this->layout = \'mylayout\';
        $this->log(__METHOD__, \'debug\');
        $this->Auth->authenticate = array(
            \'Form\' => array(
               \'userModel\' => \'User\',
               \'passwordHasher\' => array(
                    \'className\' => \'Non\',
               )
            )
        );
        $this->Auth->loginAction = array(
            \'controller\' => \'user\',
            \'action\' => \'login\',
        );
        $this->Auth->loginRedirect = \'/\';
        // or go to the home controller
        //$this->Auth->loginRedirect = \'/home\';
        $this->Auth->logoutRedirect = \'/user/login\';
    }
        ';
        if ( $insertPoint ){
            $ins = explode("\n", $insertCode);
            array_splice($buffer, $insertPoint, 0, $ins);
            $this->Controller->appController->contents = implode("\n", $buffer);
            $this->Controller->appController->dirty = true;
        }



        // If it was changed then save the file
        if ( $this->Controller->appController->dirty ){
            echo "OK. Updated AppController\n";
            file_put_contents($this->Controller->appController->file, $this->Controller->appController->contents);
            $this->Controller->appController->contents = null;
            $this->Controller->appController->loaded = false;
            $this->Controller->appController->dirty = false;
        }

    }


    public function createUserController(){
        echo "Create a UserController\n";
        file_put_contents($this->Controller->user->file, $this->userControllerCode);
    }

    public function createApiController(){
        echo "Create API Controller\n";
        // file_put_contents($this->Controller->api->file, $this->apiControllerCode);
    }

    public function createHomeController(){
        $this->Controller->home->content = $this->homeControllerCode;
        $this->Controller->home->save();
    }
    public function createHomeViews(){
        if ( ! is_dir($this->View->home->path) ){
            mkdir($this->View->home->path);
        }
        $this->View->home->index->content = '<h1>Some content</h1>';
        $this->View->home->index->save();

        // add other view files here
        
    }

    public function createUserModel(){
        echo "Create UserModel\n";
        
        $modelCode = LT . '?php
App::uses(\'AppModel\', \'Model\');
class User extends AppModel {

    public $primaryKey = \'UserId\';


    // we need a beforeSave() here to handle the password
}';
        $sql = '
/*
CREATE TABLE `user` (
  `UserId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(2048) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
*/';


        file_put_contents($this->Model->userModel->file, $modelCode . $sql);

        $this->makeClearPasswordHasher();
    }

    function makeClearPasswordHasher(){
        echo "Make a custom hasher that leaves password in the clear\n";
        if ( ! is_dir($this->ComponentAuth->nonPasswordHasher->path) ){
            // we apparently need to make the dir
            mkdir($this->ComponentAuth->nonPasswordHasher->path);
        }
        $this->ComponentAuth->nonPasswordHasher->content = $this->passwordHasherCode;
        $this->ComponentAuth->nonPasswordHasher->save();

    }

    public function createUserView(){

        // make the dir app/View/user
        mkdir($this->View->user->path);

        $viewUserIndexCode = LT . '?php
?>
<div></div>
';
        file_put_contents($this->View->user->index->file, $viewUserIndexCode);
        $viewUserLoginCode = <<<LOGINFORM
<div>
    Login
</div> 
<?php echo \$this->Form->create('User', array('action' => 'login')); ?>
    <div>
    <?php echo \$this->Form->input('username'); ?>
    </div>
    <div>
    <?php echo \$this->Form->input('password'); ?>
    </div>
    <div>
        <input type="submit">
    </div>
<?php echo \$this->Form->end(); ?>        
LOGINFORM;
        file_put_contents($this->View->user->login->file, $viewUserLoginCode);
    }

    public function homePageChanges(){
        echo "Rename home page to home1\n";
        rename($this->View->pages->home->file, $this->View->pages->home1->file);

        echo "Make a blank home page\n";
        //TODO:
        $pagesHomeCtp = '<div></div>
<div></div>
<div></div>
';
        file_put_contents($this->View->pages->home->file, $pagesHomeCtp);

    }
    public function cssChanges(){
        echo "Remove CakePHP icon from CSS\n";

        /*
        #header h1 {
        line-height:20px;
        background: #003d4c url('../img/cake.icon.png') no-repeat left;
        */
        //TODO:
        //echo ">>>>>> " . print_r($this->Css->cakeGeneric, true) . "\n";
        $this->Css->cakeGeneric->load();

        //echo ">>> css:\n" . 
        //    $this->Css->cakeGeneric->content .
        //    "\n<<<\n";

        $buffer = explode("\n", $this->Css->cakeGeneric->content );
        $lineCount = count($buffer);
        $state = 0;
        $editPoint = 0;
        //echo ">>>> checking $lineCount lines\n";
        for($i=0; $i < $lineCount; $i++){
            if ( strpos($buffer[$i], '../img/cake.icon.png') !== false ){
                //echo ">>> found cake icon in css\n";
                $buffer[$i] = str_replace(array('url(\'../img/cake.icon.png\')', 'no-repeat', 'left'), '', $buffer[$i]);
                $this->Css->cakeGeneric->dirty = true;
            }            

        }

        if ( $this->Css->cakeGeneric->dirty ){
            $this->Css->cakeGeneric->content = implode("\n", $buffer);
            $this->Css->cakeGeneric->save();
            $this->Css->cakeGeneric->clear();
        }
        else{
            echo ">>>> cake icon pattern not found\n";
        }

    }
    public function layoutChanges(){
        echo "Change title\n";  
        //TODO:
        file_put_contents($this->Layout->mylayout->file, $this->layout);
    }

    public function init($dir){
        $this->rootPath = realpath($dir);
        $this->appPath = $this->rootPath . DS . 'app';
        $this->controllerPath = $this->appPath . DS . 'Controller';
        $this->modelPath = $this->appPath . DS . 'Model';
        $this->viewPath = $this->appPath . DS . 'View';
        $this->webrootPath = $this->appPath . DS . 'webroot';
        $this->layoutPath = $this->viewPath . DS . 'Layouts';

        $this->Config = new stdclass;
        $this->Config->bootstrapFile = $this->makePath('Config', 'bootstrap.php');
        $this->Config->databaseFile = $this->makePath('Config', 'database.php');
        $this->Config->databaseDefaultFile = $this->makePath('Config', 'database.php.default');
        $this->Config->coreFile = $this->makePath('Config', 'core.php');
        $this->Config->routesFile = $this->makePath('Config', 'routes.php');

        $this->Controller = new stdclass;
        $this->Controller->appController = new stdclass;
        $this->Controller->appController->loaded = false;
        $this->Controller->appController->contents = null;
        $this->Controller->appController->dirty = false;
        $this->Controller->appController->file = $this->controllerPath . DS . 'AppController.php';

        $this->Controller->user = new stdclass;
        $this->Controller->user->loaded = false;
        $this->Controller->user->contents = null;
        $this->Controller->user->dirty = false;
        $this->Controller->user->file = $this->controllerPath . DS . 'UserController.php';

        $this->Controller->home = new AFile($this->controllerPath . DS . 'HomeController.php');

        $this->Component = new Dir($this->controllerPath . DS . 'Component');
        $this->ComponentAuth = new Dir($this->controllerPath . DS . 'Component' . DS . 'Auth');
        $this->ComponentAuth->nonPasswordHasher = new AFile($this->ComponentAuth->path . DS . 'NonPasswordHasher.php');

        $this->View = new stdclass;
        $this->View->user = new stdclass;
        $this->View->user->path = $this->viewPath . DS . 'User';  // this is really a folder

        $this->View->user->index = new stdclass;
        $this->View->user->index->file = $this->View->user->path . DS . 'index.ctp';
        $this->View->user->index->contents = null;
        $this->View->user->index->dirty = false;
        $this->View->user->index->loaded = false;
        
        $this->View->user->login = new stdclass;
        $this->View->user->login->file = $this->View->user->path . DS . 'login.ctp';
        $this->View->user->login->contents = null;
        $this->View->user->login->dirty = false;
        $this->View->user->login->loaded = false;

        $this->View->home = new Dir($this->viewPath . DS . 'Home');
        $this->View->home->index = new AFile($this->View->home->path . DS . 'index.ctp');

        $this->View->pages = new stdclass;
        $this->View->pages->path = $this->viewPath . DS . 'Pages';  // this is really a folder

        $this->View->pages->home = new stdclass;
        $this->View->pages->home->file = $this->View->pages->path . DS . 'home.ctp';
        $this->View->pages->home->contents = null;
        $this->View->pages->home->dirty = false;
        $this->View->pages->home->loaded = false;

        $this->View->pages->home1 = new stdclass;
        $this->View->pages->home1->file = $this->View->pages->path . DS . 'home1.ctp';
        $this->View->pages->home1->contents = null;
        $this->View->pages->home1->dirty = false;
        $this->View->pages->home1->loaded = false;

        // This really lives under View but we will pretend it does not
        $this->Layout = new stdclass;
        $this->Layout->path = $this->layoutPath;
        
        $this->Layout->mylayout = new stdclass;
        $this->Layout->mylayout->file = $this->Layout->path . DS . 'mylayout.ctp';
        $this->Layout->mylayout->contents = null;
        $this->Layout->mylayout->dirty = false;
        $this->Layout->mylayout->loaded = false;

        $this->Model = new stdclass;
        $this->Model->userModel = new stdclass;
        $this->Model->userModel->file = $this->modelPath . DS . 'User.php';

        $this->webroot = new Dir($this->webrootPath);

        $this->Css = new Dir($this->webroot->path . DS . 'css');
        $this->Css->cakeGeneric = new AFile($this->Css->path . DS . 'cake.generic.css');
        //echo ">>>>>1> " . print_r($this->Css, true) . "\n";

    }

    public function checkAppFolders($folders){
        foreach($folders as $folder){
            $folder = $this->appPath . DS . $folder;
            if ( ! is_dir($folder) ){
                $this->cakeOk = false;
                throw new Exception('Folder ' . $folder .' not found');
            }
            echo "OK. Found folder " . $folder . "\n"; 
        }
    }

    public function checkForCake($dir){
        echo "Checking that cake lives at {$this->appPath}\n";
        if ( ! file_exists($this->Config->bootstrapFile) ){
            throw new Exception('Bootstrap not found');
        }
        echo "OK. Found the bootstrap\n";
        if ( ! file_exists($this->Config->databaseDefaultFile) ){
            throw new Exception('Database default config not found');
        }
        echo "OK. Found {$this->Config->databaseDefaultFile}\n";
        if ( ! file_exists($this->Config->coreFile) ){
            throw new Exception('Config/core.php not found');
        }
        echo "OK. Found {$this->Config->coreFile}\n";
        if ( ! file_exists($this->Config->routesFile) ){
            throw new Exception('Config/routes.php not found');
        }
        echo "OK. Found {$this->Config->routesFile}\n";

        $this->checkAppFolders(array('Controller', 'View', 'Model'));

        if ( $this->checkShouldNotExist ){
            $dirShouldNotExist = array(
                $this->View->user->path
            );
            foreach($dirShouldNotExist as $f){
                if ( is_dir($f) ){
                    throw new Exception('Dir ' . $f . ' already exists.');
                }            
            }

            // Check for files that should not exist
            $fileShouldNotExist = array(
                $this->Model->userModel->file,
                $this->View->user->index->file,
                $this->View->user->login->file,
                $this->View->pages->home1->file,
                $this->Controller->user->file
            );

            foreach($fileShouldNotExist as $f){
                if ( file_exists($f) ){
                    throw new Exception('File ' . $f . ' already exists.');
                }            
            }
        }

        $this->cakeOk = true;
    }

    public function makePath($folder, $file){
        return $this->appPath . DS . $folder . DS . $file;
    }


    private $userControllerCode = '<?php
App::uses(\'AppController\', \'Controller\');
class UserController extends AppController{
    
    function index(){

    }

    function login(){
        if ($this->request->is(\'post\')) {
            if ($this->Auth->login()) {
                return $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Session->setFlash(
                    __(\'Username or password is incorrect\'),
                    \'default\',
                    array(),
                    \'auth\'
                );
            }
        }
    }

    public function logout(){
        return $this->redirect($this->Auth->logout());
    }

}
';
    private $homeControllerCode = '<?php
App::uses(\'AppController\', \'Controller\');
class HomeController extends AppController{
    public $uses = array();
    function index(){

    }
}
';

    private $apiControllerCode = '<?php
App::uses(\'AppController\', \'Controller\');

class ApiController extends AppController{

    public $uses = array();
    public $components = array(
        \'RequestHandler\'
        );
    
    public function beforeFilter(){
        parent::beforeFilter();
        $this->Auth->allow();
        $this->apikey = Configure::read(\'apikey\');
        $this->RequestHandler->renderAs($this, \'json\');
        $this->RequestHandler->respondAs(\'json\');
    }
    public function index(){
        $this->log(__METHOD__, \'debug\');
        $version = [\'version\' => "1.00"];
        $this->set(\'_serialize\', [\'version\']);
    }
}
';


    private $layout = '
<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */


?>
<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo $title_for_layout; ?>
    </title>
    <?php
        echo $this->Html->meta(\'icon\');

        echo $this->Html->css(\'cake.generic\');

        echo $this->fetch(\'meta\');
        echo $this->fetch(\'css\');
        echo $this->fetch(\'script\');
    ?>
</head>
<body>
    <div id="container">
        <div id="header">
            <h1>Put Title Here</h1>
        </div>
        <div id="content">

            <?php echo $this->Session->flash(); ?>

            <?php echo $this->fetch(\'content\'); ?>
        </div>
        <div id="footer">
            Footer goes here
        </div>
    </div>
    <div style="display:none">
    <?php echo $this->element(\'sql_dump\'); ?>
    </div>
</body>
</html>
';


    private $passwordHasherCode = '<?php
/**
 */

App::uses(\'AbstractPasswordHasher\', \'Controller/Component/Auth\');
App::uses(\'Security\', \'Utility\');

/**
 * Simple password hashing class.
 *
 * @package       Cake.Controller.Component.Auth
 */
class NonPasswordHasher extends AbstractPasswordHasher {

/**
 * Config for this object.
 *
 * @var array
 */
    protected $_config = array(\'hashType\' => null);

/**
 * Generates password hash.
 *
 * @param string $password Plain text password to hash.
 * @return string Password hash
 * @link http://book.cakephp.org/2.0/en/core-libraries/components/authentication.html#hashing-passwords
 */
    public function hash($password) {
        return $password;
    }

/**
 * Check hash. Generate hash for user provided password and check against existing hash.
 *
 * @param string $password Plain text password to hash.
 * @param string Existing hashed password.
 * @return boolean True if hashes match else false.
 */
    public function check($password, $hashedPassword) {
        return $hashedPassword === $this->hash($password);
    }

}

';

 }
 $pathToCake = './cakephp';
 CakePhpEnhancer::start($argv, $pathToCake);