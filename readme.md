CakePlus
--------
An enhanced version of CakePHP to get going faster.

 * Creates a UserController, Model and View.
 * Rename the standard home page.
 * Make a home controller.
 * Replaces the CakePHP icon.
 * Disable inflection.
 * Generate a database config file.
 * Modifies the AppController.
 * Implements a clear password hasher for storing passwords in the clear during the development process.