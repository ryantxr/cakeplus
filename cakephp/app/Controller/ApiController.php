<?php
App::uses('AppController', 'Controller');

class ApiController extends AppController{

    public $uses = array();
    public $components = array(
        'RequestHandler'
        );
    
    public function beforeFilter(){
        parent::beforeFilter();
        $this->Auth->allow();
        $this->apikey = Configure::read('apikey');
        $this->RequestHandler->renderAs($this, 'json');
        $this->RequestHandler->respondAs('json');
    }
    public function index(){
        $this->log(__METHOD__, 'debug');
        $version = ['version' => "1.00"];
        $this->set('_serialize', ['version']);
    }
}
