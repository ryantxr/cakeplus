cd cakephp-2.4.6
if [ -f app/Config/database.php ]; then
   echo "rm app/Config/database.php"
   rm app/Config/database.php
fi

if [ -f app/Model/User.php ]; then
   echo rm app/Model/User.php
   rm app/Model/User.php
fi

if [ -d app/View/user ]; then
	echo rm -fr app/View/user
	rm -fr app/View/user
fi

if [ -f app/View/Pages/home1.ctp ]; then
	echo rm app/View/Pages/home1.ctp
	rm app/View/Pages/home1.ctp
fi

if [ -f app/View/Layouts/mylayout.ctp ]; then
	echo rm app/View/Layouts/mylayout.ctp
	rm app/View/Layouts/mylayout.ctp
fi

if [ -f app/Controller/UserController.php ]; then
	echo rm app/Controller/UserController.php
	rm app/Controller/UserController.php
fi

if [ -f app/Controller/Component/NonPasswordHasher.php ];then
    echo rm app/Controller/Component/NonPasswordHasher.php
    rm app/Controller/Component/NonPasswordHasher.php
fi
# revert everything
git checkout -- .
